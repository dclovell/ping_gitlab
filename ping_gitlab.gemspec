# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "ping_gitlab/version"

Gem::Specification.new do |spec|
  spec.name          = "ping_gitlab"
  spec.version       = PingGitlab::VERSION
  spec.authors       = ["Douglas Lovell"]
  spec.email         = ["doug@wbreeze.com"]

  spec.summary       = %q{Reports a sampled response time from GitLab after one minute}
  spec.description   = <<-EOD
Exposes a command to check the status of https://gitlab.com and reports an average
response time after probing the site every 10 seconds for one minute.
  EOD
  spec.homepage      = 'https://gitlab.com/dclovell/ping_gitlab'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = 'http://gems.wbreeze.com'
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.require_paths = ["lib"]
  spec.bindir = 'exe'
  spec.executables << 'ping_gitlab'

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "simplecov", "~> 0.15"
end
