[![pipeline status](https://gitlab.com/dclovell/ping_gitlab/badges/master/pipeline.svg)](https://gitlab.com/dclovell/ping_gitlab/commits/master)
[![coverage report](https://gitlab.com/dclovell/ping_gitlab/badges/master/coverage.svg)](https://gitlab.com/dclovell/ping_gitlab/commits/master)

## GitLab assignment

Please write a small Ruby gem that exposes a CLI to check the status of
https://gitlab.com or https://about.gitlab.com and reports an average
response time after probing the site every 10 seconds for a one minute.

## Installation

This gem is not published. In order to run it, you will need to clone
this repository as if you were going to work on it. Here are the steps
to run the program, assuming that you already have installed
[Ruby](https://www.ruby-lang.org/en/documentation/installation/),
[RubyGems](https://rubygems.org/pages/download),
[Bundler](http://bundler.io/), and
[git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

```bash
> git clone git@gitlab.com:dclovell/ping_gitlab.git
> cd ping_gitlab
> bundle install
> rake install
> ping_gitlab
```

### Usage

The gem installs a command, `ping_gitlab`

```bash
$ ping_gitlab
Sampling https://gitlab.com ...
```

The program will then go silent for a minute while it samples retrievals from
GitLab. When it is finished, it will output a report, for example:

```bash
Completed 6 samples at 10 second intervals.
Average retrieval time is 2.649 seconds.
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

### Implementation notes

The progam runs a main thread initiated with `Runner.start`.
`Runner` coordinates with a timer thread initiated with `Timer.start`
The two coordinate through a shared `Notifier` that moderates access
to a `Calculator`.

The `Ping` class samples the target web site by timing
an HTTP or HTTPS GET request. It is programmed to follow redirects.

The timer initiates a number of "ping" requests
using `Notifier.signal`.  When it finishes the
predetermined number of signals, it exits.
The `Notifier.max_signals` method specifies the number of signals.

The runner uses the `Notifier.wait` method to block until it has
received all of the "ping" requests or an error.
When all of the pings have finished, it reports the result and exits.

The `Calculator` class executes the "ping", sums results and computes an average.
It maintains the updated value of average retrieval time that the `Runner`
reports before it exits.

The `ConsoleWriter` class abstracts output of the program to the
command line.

#### Expanding

A progress bar would help this program tremendously. To implement one,
have the `Notifier` make a callback to the `Runner` each time it sees
a completed signal in the `Notifier.wait` method. The `Runner` can then
abstract progress update through it's `ConsoleWriter`.

One very nice progress bar implementation available from RubyGems is
[`progress_bar`](https://github.com/paul/progress_bar). The `ConsoleWriter`
would hold an instance of `ProgressBar` and expose a method to `Runner`
for updating it.

## Contributing

Bug reports and pull requests are welcome on GitHub at
https://gitlab.com/dclovell/ping_gitlab
