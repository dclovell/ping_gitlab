RSpec.describe PingGitlab::Notifier do
  before :example do
    @sample_target = 3
    @calculator = instance_double(PingGitlab::Calculator)
    @notifier = PingGitlab::Notifier.new(@calculator, @sample_target)
  end

  it 'times-out without progress if not signalled' do
    expect(@calculator).to_not receive(:take_time_sample)
    expect(@calculator).to receive(:sample_count).once.and_return(0)
    @notifier.wait(2)
  end

  it 'waits for sampling and returns' do
    expect(@calculator).to receive(:take_time_sample).exactly(
      @sample_target).times.and_return(1)
    (@sample_target + 1).times do |i|
      expect(@calculator).to receive(:sample_count).and_return(i)
    end
    sample_thread = Thread.start do
      @sample_target.times do
        sleep(1)
        @notifier.signal
      end
    end
    @notifier.wait(@sample_target + 2)
    expect(sample_thread.status).to eq(false)
  end

  it 'reports its maximum signal expectation' do
    expect(@notifier.max_signals).to eq(@sample_target)
  end
end
