RSpec.describe PingGitlab::Timer do
  class MockNotifier
    attr_reader :timings, :max_signals

    def initialize(interval_target, interval_delay)
      @timings = []
      @max_signals = interval_target
      @interval_delay = interval_delay
      @prior_time = Time.now
    end

    def signal
      now = Time.now
      @timings << (now - @prior_time)
      @prior_time = now
      sleep @interval_delay
    end
  end

  it 'invokes the notifier at set intervals' do
    interval_target = 3
    time_interval = 0.2
    notifier = MockNotifier.new(interval_target, time_interval)
    timer = PingGitlab::Timer.new(notifier)
    timer.start(time_interval)
    sleep((interval_target + 2) * time_interval)
    timings = notifier.timings
    expect(timings.count).to eq(interval_target)
    (1...interval_target).each do |i|
      expect(timings[i]).to be_within(time_interval / 10.0).of(time_interval)
    end
  end
end
