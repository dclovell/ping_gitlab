RSpec.describe PingGitlab::Ping do
  context 'initialization' do
    before :context do
      @ping = PingGitlab::Ping.new
    end

    it 'provides default target' do
      expect(@ping.target_url).to eq PingGitlab::Ping::DEFAULT_TARGET
    end

    it 'sets http_status' do
      expect(@ping.http_status).to eq(0)
    end

    it 'sets timing' do
      expect(@ping.timing).to eq(0)
    end
  end

  context 'ping_once errors' do
    it 'raises an exception for a bad uri' do
      bad_uri = 'this is not a uri'
      ping = PingGitlab::Ping.new(bad_uri)
      expect(ping.target_url).to eq(bad_uri)
      expect { ping.ping_once }.to raise_exception do |error|
        expect(error.message).to match(bad_uri)
        expect(error.message).to match('is not valid')
      end
    end
  end

  context 'ping_once timing' do
    before :context do
      @ping = PingGitlab::Ping.new
      @timing = @ping.ping_once
    end

    it 'gets a valid time' do
      expect(@timing).to be > 0
    end

    it 'sets a good http status' do
      expect(@ping.http_status).to eq(200)
    end
  end
end
