RSpec.describe PingGitlab::Calculator do
  before :example do
    @pinger = instance_double(PingGitlab::Ping)
  end

  it 'invokes the ping' do
    sample_count = 3
    expect(@pinger).to receive(:ping_once).exactly(
      sample_count).times.and_return(1)
    calculator = PingGitlab::Calculator.new(@pinger)
    sample_count.times do
      calculator.take_time_sample
    end
  end

  it 'provides value of each sample' do
    timing = 1
    allow(@pinger).to receive(:ping_once).and_return(timing)
    calculator = PingGitlab::Calculator.new(@pinger)
    expect(calculator.take_time_sample).to eq timing
  end

  context 'sampling' do
    before :example do
      @timing = 1
      @sample_count = 5
      allow(@pinger).to receive(:ping_once).and_return(@timing)
      @calculator = PingGitlab::Calculator.new(@pinger)
      @sample_count.times do
        @calculator.take_time_sample
      end
    end

    it 'computes mean time of pings' do
      expect(@calculator.mean_time).to eq @timing
    end

    it 'tracks the number of samples' do
      expect(@calculator.sample_count).to eq @sample_count
    end
  end
end
