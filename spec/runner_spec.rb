RSpec.describe PingGitlab::Runner do
  before :example do
    @writer = instance_double(PingGitlab::ConsoleWriter)
  end

  it 'pings twice with short interval' do
    ping_interval = 2
    ping_time = 4
    expect(@writer).to receive(:start_message)
    expect(@writer).to receive(:summary_stats).with(
      ping_time / ping_interval, ping_interval, kind_of(Numeric)
    )
    expect(@writer).to_not receive(:exiting_on_error)
    runner = PingGitlab::Runner.new(
      ping_interval, ping_time, 'https://gitlab.com'
    )
    runner.start(@writer)
  end

  it 'fails gracefully on bad uri' do
    expect(@writer).to receive(:start_message)
    expect(@writer).to_not receive(:summary_stats)
    expect(@writer).to receive(:exiting_on_error)
    runner = PingGitlab::Runner.new(1, 3, 'not a uri')
    runner.start(@writer)
  end
end
