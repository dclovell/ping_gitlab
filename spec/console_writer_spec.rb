RSpec::Matchers.define :multi_match do |*strings|
  match do |string|
    strings.reduce(true) do |memo, expected|
      memo && string.include?(expected)
    end
  end
end

RSpec.describe PingGitlab::ConsoleWriter do
  before :example do
    @output = instance_double(IO)
    @writer = PingGitlab::ConsoleWriter.new(@output)
  end

  it 'formats target uri on start message' do
    target = 'https://gitlab.com'
    expect(@output).to receive(:puts).with(match(target))
    @writer.start_message(target)
  end

  it 'formats stats on summary_stats message' do
    sample_count = 4
    interval = 15
    mean_time = 2.84
    expect(@output).to receive(:puts).with(
      multi_match(sample_count.to_s, interval.to_s, mean_time.to_s)
    )
    @writer.summary_stats(sample_count, interval, mean_time)
  end
end
