RSpec.describe PingGitlab do
  it 'has a version number' do
    expect(PingGitlab::VERSION).not_to be nil
  end

  it 'has an entry point' do
    expect(PingGitlab::Runner.singleton_methods).to include(:invoke)
  end
end
