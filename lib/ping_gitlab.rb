require 'ping_gitlab/version'
require 'ping_gitlab/console_writer'
require 'ping_gitlab/runner'

module PingGitlab
  # runs a status check on 'https://gitlab.com'
  # over a minute, with a sample every ten seconds
  # and outputs to the standard output
  class Runner
    def self.invoke
      writer = ConsoleWriter.new($stdout)
      runner = Runner.new(10, 60, 'https://gitlab.com')
      runner.start(writer)
    end
  end
end
