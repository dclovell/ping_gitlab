module PingGitlab
  # Signal a notifier at regular intervals,
  # for count taken from the notifier
  class Timer
    # initialize with a Notifier that will get pinged on interval with an
    #   invocation of its signal method
    def initialize(notifier)
      @notifier = notifier
      @max_signals = notifier.max_signals
    end

    # start pinging the notifier at specified interval
    # stops upon reaching the notifier specified max_signals count
    def start(interval)
      @thread = Thread.start do
        @max_signals.times do
          # call asynchronously to keep the interval
          Thread.start { @notifier.signal }
          sleep(interval)
        end
      end
    end

    # stop the timer prematurely
    def stop
      @thread.stop
    end
  end
end
