require 'ping_gitlab/ping'
require 'ping_gitlab/calculator'
require 'ping_gitlab/notifier'
require 'ping_gitlab/timer'

module PingGitlab
  # Coordinate the activities of a Calculator, Notifier, and Timer
  # in order to run a status check
  class Runner
    # ping address on interval for time_limit, then report
    def initialize(interval, time_limit, address)
      @time_limit = time_limit
      @address = address
      ping = Ping.new(address)
      @calculator = Calculator.new(ping)
      expected_sample_size = (time_limit / interval).to_i
      @notifier = Notifier.new(@calculator, expected_sample_size)
      @timer = Timer.new(@notifier)
      @interval = interval
    end

    def start(writer)
      thread_abort_state = Thread.abort_on_exception
      Thread.abort_on_exception = true
      execute_status_check(writer)
    rescue RuntimeError => e
      writer.exiting_on_error(e.message)
    ensure
      Thread.abort_on_exception = thread_abort_state
    end

    private

    def execute_status_check(writer)
      writer.start_message(@address)
      @timer.start(@interval)
      @notifier.wait(@time_limit + @interval)
      writer.summary_stats(
        @calculator.sample_count, @interval, @calculator.mean_time
      )
    end
  end
end
