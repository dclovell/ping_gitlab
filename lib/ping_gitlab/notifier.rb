module PingGitlab
  # Orchestrate access to a calculator between
  # signaling a sample ping from a timer and
  # waiting for completion of sampling
  class Notifier
    DEFAULT_TIMEOUT = 120

    attr_reader :max_signals

    # Initialize the notifier with a calculator and target sample count
    # The calculator must respond to take_time_sample and sample_count.
    def initialize(calculator, target_count)
      @calculator = calculator
      @max_signals = target_count
      @mutex = Mutex.new
      @condition = ConditionVariable.new
      @signal_seen = false
    end

    # Ask the calculator to take a sample and then signal for completion check
    def signal
      @mutex.synchronize do
        @signal_seen = true
        @calculator.take_time_sample
        @condition.broadcast
      end
    end

    # Wait until the calculator has reached target count and then return
    # timeout is the maximum time to wait. defaults to two minutes
    def wait(timeout = DEFAULT_TIMEOUT)
      @mutex.synchronize do
        @signal_seen = true
        while @signal_seen && @calculator.sample_count < max_signals
          @signal_seen = false
          @condition.wait(@mutex, timeout)
        end
      end
    end
  end
end
