module PingGitlab
  # Use an instance of Ping to make measurements and aggregate the results
  class Calculator
    attr_reader :sample_count

    # initialize the calculator with a Ping
    def initialize(pinger)
      @pinger = pinger
      @sample_count = 0
      @total = 0
    end

    # ping once and accumulate the result
    # return the individual sample time
    def take_time_sample
      sample = @pinger.ping_once
      @total += sample
      @sample_count += 1
      sample
    end

    # return the computed mean time of all samples
    def mean_time
      @total.to_f / sample_count
    end
  end
end
