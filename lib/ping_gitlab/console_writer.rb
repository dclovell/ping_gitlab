module PingGitlab
  # Write status messages and report to given output
  class ConsoleWriter
    def initialize(output = $stdout)
      @out = output
    end

    def start_message(target_uri)
      @out.puts "Sampling #{target_uri} ..."
    end

    def summary_stats(sample_count, interval, mean_time)
      @out.puts "Completed #{sample_count} samples " \
        "at #{interval} second intervals.\n" \
        "Average retrieval time is #{mean_time.round(3)} seconds."
    end

    def exiting_on_error(error)
      @out.puts "Failed on error, #{error}"
    end
  end
end
