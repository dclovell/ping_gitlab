require 'uri'
require 'net/http'

module PingGitlab
  # Retrieve a url and measure time for retrieval
  class Ping
    DEFAULT_TARGET = 'https://gitlab.com'.freeze

    attr_accessor :target_url
    attr_reader :http_status, :timing

    def initialize(target_url = nil)
      self.target_url = target_url || DEFAULT_TARGET
      reset
    end

    # sets http_status and timing
    # returns timing
    # raises StandardError with message on failure
    def ping_once
      reset
      start_time = Time.now
      @http_status = fetch_resource(target_url)
      end_time = Time.now
      @timing = end_time - start_time
    end

    private

    def reset
      @http_status = 0
      @timing = 0
    end

    def fetch_resource(target, limit = 3)
      response = get_response(target)
      status = response.code.to_i
      if 300 < status && 1 < limit && [301, 302, 303, 307, 308].include?(status)
        status = fetch_resource(response['location'], limit - 1)
      end
      status
    end

    def get_response(target)
      uri = URI.parse(target)
      Net::HTTP.get_response(uri)
    rescue URI::InvalidURIError
      raise "Configured #{target_url} is not valid"
    end
  end
end
